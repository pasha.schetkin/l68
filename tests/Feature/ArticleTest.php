<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ArticleTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     * @group article
     * @return void
     */
    public function test_success_create()
    {
        $user = User::factory()->create();
        $data = [
            'title' => $this->faker->sentence(),
            'content' => $this->faker->paragraph(),
            'user_id' => $user->id
        ];
        $this->assertDatabaseMissing('articles', $data);

        $response = $this->postJson(route('articles.store'), $data);

        $response->assertCreated();
        $payload = $response->collect()->toArray();
        $this->assertArrayHasKey('data', $payload);
        $this->assertArrayHasKey('id', $payload['data']);
        $this->assertArrayHasKey('title', $payload['data']);
        $this->assertArrayHasKey('author', $payload['data']);
        $this->assertArrayHasKey('id', $payload['data']['author']);
        $this->assertArrayHasKey('email', $payload['data']['author']);
        $this->assertArrayHasKey('comments', $payload['data']);

        $this->assertDatabaseHas('articles', $data);
        $this->assertEquals($data['title'], $payload['data']['title']);
    }
}
